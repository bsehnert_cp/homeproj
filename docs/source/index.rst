.. Home Projects documentation master file, created by
   sphinx-quickstart on Fri Sep  7 20:17:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Home Projects
================

.. toctree::
	:maxdepth: 2
	:caption: Contents:
	:numbered:

	sprinkers/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
